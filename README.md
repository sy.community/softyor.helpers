# Helpers
This project is a collection of useful scripts which are commonly used for different projects.
If you need to contribute please raise PR.

#### Git Helpers

| Name | Location/File | Description |
| ------ | ------ | ----- |
| Multiple Git Push Utitlity | git\git-multiple.bat | This bat will help you to push to multiple git repo at one go. Open the bat file and change the entries accordingly. After you clone a repo run this to set this up, and henceforth you do *git push* |







#### Contributers
 - [SoftYor Solutions](http://www.softyor.com/)
