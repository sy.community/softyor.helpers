@echo OFF
echo .
echo Initializing GIT with multiple repo
echo .
git config user.name "SoftYor Solutions"
git config user.email "yor.prof.soft@gmail.com"
git remote set-url --add --push origin https://gitlab.com/sy.poc/git-main.git
git remote set-url --add --push origin https://gitlab.com/sy.poc/git-other.git
echo .
echo ...Done
echo Use 'git push' to push in all repo simultanously
echo .
